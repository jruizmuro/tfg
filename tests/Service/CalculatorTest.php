<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Service\Calculator;

class CalculatorTest extends WebTestCase
{
    public function testSum()
    {
        $exampleA = 1;
        $exampleB = 2;
        $result = 3;

        $calculator = new Calculator();
        $this->assertEquals($result, $calculator->sum($exampleA,$exampleB));
    }
}