<?php

namespace App\Service;

class Calculator
{
    public function sum(float $a, float $b): float
    {
        return $a+$b;
    }
}