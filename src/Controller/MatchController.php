<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Matches;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;


class MatchController extends AbstractController
{
    #[Route('/matches', name: 'app_matches')]
    public function indexMatches(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Matches::class)->findAll();
        $user = $em->getRepository(User::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        return $this->render('adminReserve/matches.html.twig', ['pagination' => $pagination, 'list' => $user ]);
    }
}
