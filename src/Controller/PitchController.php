<?php

namespace App\Controller;

use App\Entity\Club;
use App\Entity\Pitch;
use App\Form\PitchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class PitchController extends AbstractController
{
    #[Route('/pitches/{id}', name: 'app_pitch')]
    public function indexPitch(EntityManagerInterface $em, string $id){
       
        $query = $em->getRepository(Club::class)->findAll();
        $club = $em->getRepository(Club::class)->find($id);
        
        return $this->render('adminPitch/pitch.html.twig',[ 
            'club' => $club, 'list' => $query
        ]);
    }

    #[Route('/pitchadd/{id}', name: 'app_pitch_add')]
    public function indexAdPitch(Request $request, EntityManagerInterface $entityManager): Response
    {
        $pitch = new Pitch();
        $form = $this->createForm(PitchType::class, $pitch);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($pitch);
            $entityManager->flush();
            $this->addFlash('alerta', '¡¡Pista creada satisfactoriamente!!');

            return $this->redirectToRoute('app_admin_clubs');
        }

        $club = $entityManager->getRepository(Club::class)->findAll();
        $query = $entityManager->getRepository(Pitch::class)->findAll();

        return $this->render('adminPitch/add_pitch.html.twig', [
            'addForm' => $form->createView(), 'list' => $query, 'list2' => $club
        ]);
    }

    #[Route('/pitchupdate/{id}', name: 'app_pitch_update')]
    public function indexUpdatePitch(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine ,$id){

        $updatePitch = $doctrine->getRepository(Pitch::class)->find($id);
        $form = $this->createForm(PitchType::class, $updatePitch);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Pista actualizada satisfactoriamente!!');

           return $this->redirectToRoute('app_admin_clubs');
        }

        $query = $em->getRepository(Pitch::class)->findAll();
        
        return $this->render('adminPitch/update_pitch.html.twig',[
            'form' => $form->createView(), 'list' => $query
        ]);
    }

    #[Route('/pitchdelete/{id}', name: 'app_pitch_delete')]
    public function indexDeletePitch(EntityManagerInterface $em, PersistenceManagerRegistry $doctrine ,$id){

        $data = $doctrine->getRepository(Pitch::class)->find($id);
        $em = $doctrine->getManager();
        $em->remove($data);
        $em->flush();

        $this->addFlash('alerta', '¡¡Pista eliminada satisfactoriamente!!');

        return $this->redirectToRoute('app_admin_clubs');
    }
}
