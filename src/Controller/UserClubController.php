<?php

namespace App\Controller;

use App\Entity\Club;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;


class UserClubController extends AbstractController
{
    #[Route('/userclubs', name: 'app_user_clubs')]
    public function indexUserClub(EntityManagerInterface $em): Response
    {
        $query = $em->getRepository(Club::class)->findAll();

        return $this->render('user/user_clubs/clubs_user.html.twig', ['list' => $query]);
    }
}
