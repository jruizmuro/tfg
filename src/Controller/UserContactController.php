<?php

namespace App\Controller;

use App\Entity\Question;
use App\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class UserContactController extends AbstractController
{
    /**
     * @Route("/contact", name="app_contact")
     * @param EntityManager $em
     */
    public function indexContact(EntityManagerInterface $em, Request $request): Response
    {
        $question = new Question();
        $form = $this->createForm(ContactFormType::class, $question);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($question);
            $em->flush();

            return $this->redirectToRoute('app_main');
        }

        return $this->render('user/user_contact/contact.html.twig', [
            'contactForm' => $form->createView()
        ]);
    }
}
