<?php

namespace App\Controller;

use App\Entity\Club;
use App\Entity\Reserve;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class ReserveController extends AbstractController
{
    #[Route('/reserve', name: 'app_reserve')]
    public function indexReserve(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Reserve::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        $query = $em->getRepository(Club::class)->findAll();
        
        return $this->render('main/reserve.html.twig', ['pagination' => $pagination, 'list' => $query ]);
    }
}
