<?php

namespace App\Controller;

use App\Entity\Training;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;


class TrainingController extends AbstractController
{
    #[Route('/trainings', name: 'app_trainings')]
    public function indexTraining(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Training::class)->findAll();
        $user = $em->getRepository(User::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        return $this->render('adminReserve/trainings.html.twig', ['pagination' => $pagination, 'list' => $user ]);
    }
}
