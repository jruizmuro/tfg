<?php

namespace App\Controller;

use App\Entity\Club;
use App\Form\ClubType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;


class ClubController extends AbstractController
{

    #[Route('/adminclubs', name: 'app_admin_clubs')]
    public function indexClub(EntityManagerInterface $em): Response
    {

        $query = $em->getRepository(Club::class)->findAll();

        return $this->render('adminClub/clubs_admin.html.twig', ['list' => $query]);
    }

    #[Route('/clubsadd', name: 'app_club_add')]
    public function indexAddClub(Request $request, EntityManagerInterface $entityManager): Response
    {
        $club = new Club();
        $form = $this->createForm(ClubType::class, $club);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager->persist($club);
            $entityManager->flush();
            $this->addFlash('alerta', '¡¡Club añadido satisfactoriamente!!');

            return $this->redirectToRoute('app_admin_clubs');
        }

        $query = $entityManager->getRepository(Club::class)->findAll();

        return $this->render('adminClub/add_clubs.html.twig', [
            'addForm' => $form->createView(), 'list' => $query
        ]);
    }

    #[Route('/clubupdate/{id}', name: 'app_club_update')]
    public function indexUpdateClub(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine ,$id){

        $updateClub = $doctrine->getRepository(Club::class)->find($id);
        $form = $this->createForm(ClubType::class, $updateClub);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Club actualizado satisfactoriamente!!');

           return $this->redirectToRoute('app_admin_clubs');
        }

        $query = $em->getRepository(Club::class)->findAll();
        
        return $this->render('adminClub/update_club.html.twig',[
            'form' => $form->createView(), 'list' => $query
        ]);
    }

    #[Route('/clubdelete/{id}', name: 'app_club_delete')]
    public function deleteclub(PersistenceManagerRegistry $doctrine, $id, EntityManagerInterface $em){
        
        //$pitchdelete = $em-> createQuery("DELETE FROM App\Entity\Pitch p WHERE p.id_club = $id");
        //$query = $pitchdelete->getResult();
        $data = $doctrine->getRepository(Club::class)->find($id);
        $em = $doctrine->getManager();
        $em->remove($data);
        $em->flush();

        $this->addFlash('alerta', '¡¡Club eliminado satisfactoriamente!!');

        return $this->redirectToRoute('app_admin_clubs');

    }

}