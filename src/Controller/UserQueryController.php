<?php

namespace App\Controller;

use App\Entity\Question;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class UserQueryController extends AbstractController
{
    #[Route('/userquery/{id}', name: 'app_user_query')]
    public function indexUserQuery(EntityManagerInterface $em, Request $request, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Question::class);
        $allAppointmentsQuery = $allQuery->createQueryBuilder('q')
            ->where('q.email != :email')
            ->setParameter('email', 'canceled')
            ->getQuery();

        $pagination = $paginator->paginate(
            $allAppointmentsQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('user/user_query/user_query.html.twig', ['pagination' => $pagination ]);
    }
}
