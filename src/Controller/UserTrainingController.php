<?php

namespace App\Controller;

use App\Entity\Training;
use App\Form\TrainingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;



class UserTrainingController extends AbstractController
{
    #[Route('/usertraining', name: 'app_user_training')]
    public function indexUserTraining(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Training::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        return $this->render('user/user_trainings/trainings.html.twig', ['pagination' => $pagination ]);
    }

    #[Route('/userupdatetraining/{id}', name: 'app_training_update')]
    public function indexUpdateClub(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine ,$id){

        $updatetraining = $doctrine->getRepository(Training::class)->find($id);
        $form = $this->createForm(TrainingType::class, $updatetraining);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Entreno actualizado satisfactoriamente!!');

           return $this->redirectToRoute('app_user_training');
        }
        
        return $this->render('user/user_trainings/training_update.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
