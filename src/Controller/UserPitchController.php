<?php

namespace App\Controller;

use App\Entity\Club;
use App\Entity\Pitch;
use App\Entity\Training;
use App\Form\TrainingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;


class UserPitchController extends AbstractController
{
    #[Route('/userpitches/{id}', name: 'app_user_pitch')]
    public function indexUserPitch(EntityManagerInterface $em, string $id){
       
        $club = $em->getRepository(Club::class)->find($id);
        $query = $em->getRepository(Club::class)->findAll();
        
        return $this->render('user/user_pitch/pitch.html.twig',[ 
            'club' => $club, 'list' => $query
        ]);
    }

    #[Route('/playpitchmatch/{id}', name: 'app_user_pitch_match')]
    public function indexMatchPitch(EntityManagerInterface $em){
       
        $query = $em->getRepository(Club::class)->findAll();
        
        return $this->render('user/user_pitch/pitch_match.html.twig',[ 
            'list' => $query
        ]);
    }

    #[Route('/trainingpitch/{id}', name: 'app_user_pitch_training')]
    public function indexTrainingPitch(EntityManagerInterface $em, Request $request, $id){
       
        $training = new Training();
        $form = $this->createForm(TrainingType::class, $training);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($training);
            $em->flush();
            $this->addFlash('alerta', '¡¡Entrenamiento concretado satisfactoriamente!!');

            return $this->redirectToRoute('app_user_training');
        }

        $pitch = $em->getRepository(Pitch::class)->find($id);
        
        return $this->render('user/user_pitch/pitch_training.html.twig',[ 
            'addForm' => $form->createView(), 'pitch' => $pitch
        ]);
    }
}
