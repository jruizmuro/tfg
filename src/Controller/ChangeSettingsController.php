<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserLowRolType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class ChangeSettingsController extends AbstractController
{
    #[Route('/setting/{id}', name: 'app_settings')]
    public function indexSetting(EntityManagerInterface $em, PersistenceManagerRegistry $doctrine ,$id, Request $request, UserPasswordHasherInterface $userPasswordHasher){
        $updateUser = $doctrine->getRepository(User::class)->find($id);
        $form = $this->createForm(UserLowRolType::class, $updateUser);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $updateUser->setPassword(
                $userPasswordHasher->hashPassword(
                        $updateUser,
                        $form->get('password')->getData()
                    )
                );
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Registro actualizado satisfactoriamente!!');

           return $this->redirectToRoute('app_logout');
        }

        return $this->render('user/update_profile/update_user.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
