<?php

namespace App\Controller;

use App\Entity\Question;
use App\Form\MakeResponseType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;

class QueryController extends AbstractController
{
    /**
     * @Route("/query", name="app_query")
     * @param EntityManager $em
     */
    public function indexQuery(EntityManagerInterface $em, Request $request, PaginatorInterface $paginator): Response
    {

        $allQuery = $em->getRepository(Question::class);
        $allAppointmentsQuery = $allQuery->createQueryBuilder('q')
            ->where('q.email != :email')
            ->setParameter('email', 'canceled')
            ->getQuery();

        $pagination = $paginator->paginate(
            $allAppointmentsQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        

        return $this->render('adminQuery/query.html.twig', ['pagination' => $pagination ]);
    }

    #[Route('/response/{id}', name: 'app_response_query')]
    public function indexUserQuery(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine, $id): Response
    {
        $response = $doctrine->getRepository(Question::class)->find($id);
        $form = $this->createForm(MakeResponseType::class, $response);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $doctrine->getManager();
            $em->flush();

           return $this->redirectToRoute('app_query');
        }

        return $this->render('adminQuery/response.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
