<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class UserAdminController extends AbstractController
{

    /**
     * @Route("/userlist", name="app_user_list")
     * @param EntityManager $em
     */
    public function indexUserList(EntityManagerInterface $em, Request $request, PaginatorInterface $paginator): Response
    {

        $allQuery = $em->getRepository(User::class);
        $allAppointmentsQuery = $allQuery->createQueryBuilder('u')
            ->where('u.email != :email')
            ->setParameter('email', 'canceled')
            ->getQuery();

        $pagination = $paginator->paginate(
            $allAppointmentsQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        return $this->render('adminUser/user_list.html.twig', ['pagination' => $pagination]);
    }

    #[Route('/add', name: 'app_add')]
    public function indexAdd(EntityManagerInterface $em, Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('password')->getData(),
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('alerta', '¡¡Usuario añadido satisfactoriamente!!');

            return $this->redirectToRoute('app_user_list');
        }

        return $this->render('adminUser/add_user.html.twig', [
            'addForm' => $form->createView()
        ]);
    }

    #[Route('/update/{id}', name: 'app_update')]
    public function indexUpdate(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine, UserPasswordHasherInterface $userPasswordHasher ,$id){

        $updateUser = $doctrine->getRepository(User::class)->find($id);
        $form = $this->createForm(UserType::class, $updateUser);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $updateUser->setPassword(
                $userPasswordHasher->hashPassword(
                        $updateUser,
                        $form->get('password')->getData()
                    )
                );
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Usuario actualizado satisfactoriamente!!');

           return $this->redirectToRoute('app_user_list');
        }

        $userName = $em->getRepository(User::class)->findAll();

        return $this->render('adminUser/update_user.html.twig',[
            'form' => $form->createView(), 'list' => $userName
        ]);
    }

    #[Route('/userdelete/{id}', name: 'app_user_delete')]
    public function deleteuser(PersistenceManagerRegistry $doctrine, $id, EntityManagerInterface $em){
        
        $data = $doctrine->getRepository(User::class)->find($id);
        $em = $doctrine->getManager();
        $em->remove($data);
        $em->flush();

        $this->addFlash('alerta', '¡¡Usuario eliminado satisfactoriamente!!');

        return $this->redirectToRoute('app_user_list');
    }

}