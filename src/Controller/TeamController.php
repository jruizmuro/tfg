<?php

namespace App\Controller;

use App\Entity\Team;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;

class TeamController extends AbstractController
{
    #[Route('/teams', name: 'app_teams')]
    public function indexTeams(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Team::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('adminReserve/teams.html.twig', ['pagination' => $pagination ]);
    }
}
