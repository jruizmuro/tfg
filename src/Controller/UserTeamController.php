<?php

namespace App\Controller;

use App\Entity\Team;
use App\Form\UserTeamType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;


class UserTeamController extends AbstractController
{
    #[Route('/usercreateam', name: 'app_user_team')]
    public function indexUserTeam(Request $request, EntityManagerInterface $em, PersistenceManagerRegistry $doctrine): Response
    {
        
        $team = new Team();
        $form = $this->createForm(UserTeamType::class, $team);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($team);
            $em->flush();

            return $this->redirectToRoute('app_main');
        }
        
        return $this->render('user/user_team/create_team.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
