<?php

namespace App\Controller;

use App\Entity\Matches;
use App\Form\MatchesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Persistence\ManagerRegistry as PersistenceManagerRegistry;


class UserMatchController extends AbstractController
{
    #[Route('/usermatch', name: 'app_user_matches')]
    public function indexUserMatches(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $allQuery = $em->getRepository(Matches::class)->findAll();
        
        $pagination = $paginator->paginate(
            $allQuery, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );
        
        return $this->render('user/user_match/matches.html.twig', ['pagination' => $pagination ]);
    }

    #[Route('/userplaymatch', name: 'app_user_playmatches')]
    public function indexUserPlayMatches(Request $request, EntityManagerInterface $em, PaginatorInterface $paginator): Response
    {
        $match = new Matches();
        $form = $this->createForm(MatchesType::class, $match);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->persist($match);
            $em->flush();
            $this->addFlash('alerta', '¡¡Partido añadido satisfactoriamente!!');

            return $this->redirectToRoute('app_user_matches');
        }

        return $this->render('user/user_match/play_match.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/usermatchupdate/{id}', name: 'app_matches_update')]
    public function indexUpdateMatch(EntityManagerInterface $em, Request $request, PersistenceManagerRegistry $doctrine ,$id){

        $updatematch = $doctrine->getRepository(Matches::class)->find($id);
        $form = $this->createForm(MatchesType::class, $updatematch);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()){
            $em = $doctrine->getManager();
            $em->flush();
            $this->addFlash('alerta', '¡¡Partido actualizado satisfactoriamente!!');

           return $this->redirectToRoute('app_user_matches');
        }
        
        return $this->render('user/user_match/matches_update.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
