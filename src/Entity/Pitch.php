<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\PitchRepository;
use Symfony\Component\Uid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=PitchRepository::class)
 */
class Pitch
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;


    /**
     * @ORM\ManyToOne(targetEntity="Club", inversedBy="pitchs")
     * @ORM\JoinColumn()
     */
    private ?Club $club = null;

    /**
     * @ORM\OneToMany(targetEntity="Training", mappedBy="pitch")
     */
    private Collection $training;

    public function __construct()
    {
        $this->training = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClub(): ?Club
    {
        return $this->club;
    }

    public function setClub(Club $club): self
    {
        $this->club = $club;

        return $this;
    }

    public function getTraining(): Collection
    {
        return $this->training;
    }

    public function __toString()
    {
        return $this->name;
    }
}
