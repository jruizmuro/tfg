<?php

namespace App\Entity;

use App\Repository\TeamMatchRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=TeamMatchRepository::class)
 */
class TeamMatch
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id_team;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id_matches;


    public function getId()
    {
        return $this->id;
    }

    public function getIdTeam()
    {
        return $this->id_team;
    }

    public function setIdTeam($id_team): self
    {
        $this->id_team = $id_team;

        return $this;
    }

    public function getIdMatches()
    {
        return $this->id_matches;
    }

    public function setIdMatches($id_matches): self
    {
        $this->id_matches = $id_matches;

        return $this;
    }
    
}
