<?php

namespace App\Entity;

use App\Repository\TeamPlayerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Uuid;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=TeamPlayerRepository::class)
 */
class TeamPlayer
{
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private $id;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id_team;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private $id_user;

    public function getId()
    {
        return $this->id;
    }

    public function getIdTeam()
    {
        return $this->id_team;
    }

    public function setIdTeam($id_team): self
    {
        $this->id_team = $id_team;

        return $this;
    }

    public function getIdUser()
    {
        return $this->id_user;
    }

    public function setIdUser($id_user): self
    {
        $this->id_user = $id_user;

        return $this;
    }
}
