<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220520084238 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training ADD pitch_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', DROP id_pitch');
        $this->addSql('ALTER TABLE training ADD CONSTRAINT FK_D5128A8FFEEFC64B FOREIGN KEY (pitch_id) REFERENCES pitch (id)');
        $this->addSql('CREATE INDEX IDX_D5128A8FFEEFC64B ON training (pitch_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE training DROP FOREIGN KEY FK_D5128A8FFEEFC64B');
        $this->addSql('DROP INDEX IDX_D5128A8FFEEFC64B ON training');
        $this->addSql('ALTER TABLE training ADD id_pitch CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP pitch_id');
    }
}
