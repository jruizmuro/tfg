<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427081227 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE matches (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date DATE NOT NULL, result TINYINT(1) NOT NULL, id_pitch CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_match (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_team CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_matches CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', date DATE NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE team_player (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_team CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_user CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE training (id CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', description VARCHAR(255) NOT NULL, date DATE NOT NULL, status TINYINT(1) NOT NULL, id_user CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_trainer CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', id_pitch CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE matches');
        $this->addSql('DROP TABLE team');
        $this->addSql('DROP TABLE team_match');
        $this->addSql('DROP TABLE team_player');
        $this->addSql('DROP TABLE training');
    }
}
