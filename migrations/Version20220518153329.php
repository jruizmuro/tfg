<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220518153329 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pitch ADD club_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:uuid)\', DROP id_club');
        $this->addSql('ALTER TABLE pitch ADD CONSTRAINT FK_279FBED961190A32 FOREIGN KEY (club_id) REFERENCES club (id)');
        $this->addSql('CREATE INDEX IDX_279FBED961190A32 ON pitch (club_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE pitch DROP FOREIGN KEY FK_279FBED961190A32');
        $this->addSql('DROP INDEX IDX_279FBED961190A32 ON pitch');
        $this->addSql('ALTER TABLE pitch ADD id_club CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', DROP club_id');
    }
}
