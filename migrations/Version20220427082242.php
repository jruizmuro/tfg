<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427082242 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matches CHANGE result result VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE pitch CHANGE status status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE reserve DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE reserve ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE team_match DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE team_match ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE team_player DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE team_player ADD PRIMARY KEY (id)');
        $this->addSql('ALTER TABLE training CHANGE status status VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE matches CHANGE result result TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE pitch CHANGE status status VARCHAR(1) NOT NULL');
        $this->addSql('ALTER TABLE reserve DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE reserve ADD PRIMARY KEY (id, id_user, id_pitch)');
        $this->addSql('ALTER TABLE team_match DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE team_match ADD PRIMARY KEY (id, id_team, id_matches)');
        $this->addSql('ALTER TABLE team_player DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE team_player ADD PRIMARY KEY (id, id_team, id_user)');
        $this->addSql('ALTER TABLE training CHANGE status status TINYINT(1) NOT NULL');
    }
}
