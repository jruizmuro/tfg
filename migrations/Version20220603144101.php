<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220603144101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX id_usuario ON matches');
        $this->addSql('DROP INDEX id_usuario_2 ON matches');
        $this->addSql('DROP INDEX UNIQ_62615BAFCF8192D ON matches');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX id_usuario ON matches (id_usuario)');
        $this->addSql('CREATE INDEX id_usuario_2 ON matches (id_usuario)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_62615BAFCF8192D ON matches (id_usuario)');
    }
}
