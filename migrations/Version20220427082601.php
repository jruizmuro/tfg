<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220427082601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FE0EA226B3CA4B ON reserve (id_user)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1FE0EA22C91AFFE9 ON reserve (id_pitch)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD5D8C454FC0BA1D ON team_match (id_team)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BD5D8C45D8BBBCCA ON team_match (id_matches)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EE023DBC4FC0BA1D ON team_player (id_team)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EE023DBC6B3CA4B ON team_player (id_user)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_1FE0EA226B3CA4B ON reserve');
        $this->addSql('DROP INDEX UNIQ_1FE0EA22C91AFFE9 ON reserve');
        $this->addSql('DROP INDEX UNIQ_BD5D8C454FC0BA1D ON team_match');
        $this->addSql('DROP INDEX UNIQ_BD5D8C45D8BBBCCA ON team_match');
        $this->addSql('DROP INDEX UNIQ_EE023DBC4FC0BA1D ON team_player');
        $this->addSql('DROP INDEX UNIQ_EE023DBC6B3CA4B ON team_player');
    }
}
