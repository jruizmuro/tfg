<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220601083125 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE team_player_team');
        $this->addSql('ALTER TABLE team_player ADD id_team CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EE023DBC4FC0BA1D ON team_player (id_team)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE team_player_team (team_player_id CHAR(36) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:uuid)\', team_id CHAR(36) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci` COMMENT \'(DC2Type:uuid)\', INDEX IDX_8DCA19612EE3EB38 (team_player_id), INDEX IDX_8DCA1961296CD8AE (team_id), PRIMARY KEY(team_player_id, team_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE team_player_team ADD CONSTRAINT FK_8DCA1961296CD8AE FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE team_player_team ADD CONSTRAINT FK_8DCA19612EE3EB38 FOREIGN KEY (team_player_id) REFERENCES team_player (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX UNIQ_EE023DBC4FC0BA1D ON team_player');
        $this->addSql('ALTER TABLE team_player DROP id_team');
    }
}
