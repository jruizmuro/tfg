<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220516112843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reserve CHANGE date date DATE NOT NULL');
        $this->addSql('ALTER TABLE training DROP id_trainer');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reserve CHANGE date date DATETIME NOT NULL');
        $this->addSql('ALTER TABLE training ADD id_trainer CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
    }
}
